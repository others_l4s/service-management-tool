﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Configuration;
using Microsoft.Web.Administration;
using Microsoft.SqlServer.Management.Smo;

namespace ServiceManagementWindowsService
{
	public partial class Service1 : ServiceBase
	{
		public Service1()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			FileSystemWatcher watcher = new FileSystemWatcher();
			watcher.Path = ConfigurationManager.AppSettings["ServiceCommandWatcherFolderPath"];
			watcher.NotifyFilter = NotifyFilters.FileName;
			watcher.Filter = "*.txt";

			// Add event handlers.
			watcher.Created += new FileSystemEventHandler(OnChanged);
			// Begin watching.
			watcher.EnableRaisingEvents = true;

			GetAllApplicationPoolNames();
		}

		protected override void OnStop()
		{
		}

		private void OnChanged(object sender, FileSystemEventArgs e)
		{
			ReadFileAndProcess(e.FullPath);
		}

		protected override void OnCustomCommand(int command)
		{
			base.OnCustomCommand(command);
			if (command == (int)Commands.StartSQL)
			{
				this.EventLog.WriteEntry("Command execute " + command + "");
			}
		}

		#region ' Private Methods'

		//Read command file and execute as per command 
		private void ReadFileAndProcess(string filePath)
		{
			try
			{
				System.Threading.Thread.Sleep(1000);
				string[] lines = File.ReadAllLines(filePath);
				if (lines.Length < 1)
				{
					return;
				}
				var commandTextValues = lines[0].Split(' ');
				if (commandTextValues.Length < 1)
				{
					return;
				}
				var command = Convert.ToInt32(commandTextValues[0]);
				var extraArgs = commandTextValues.Length > 1 ? lines[0].Substring(lines[0].IndexOf(' ') + 1) : string.Empty;
				var serviceName = string.Empty;
				switch (command)
				{
					case (int)Commands.StartSQL:
						serviceName = ConfigurationManager.AppSettings["SQLServiceName"];
						StartService(serviceName);
						break;
					case (int)Commands.StopSQL:
						serviceName = ConfigurationManager.AppSettings["SQLServiceName"];
						StopService(serviceName);
						break;
					case (int)Commands.RestartSQL:
						serviceName = ConfigurationManager.AppSettings["SQLServiceName"];
						RestartService(serviceName);
						break;
					case (int)Commands.StartIIS:
						serviceName = ConfigurationManager.AppSettings["IISServiceName"];
						StartService(serviceName);
						break;
					case (int)Commands.StopIIS:
						serviceName = ConfigurationManager.AppSettings["IISServiceName"];
						StopService(serviceName);
						break;
					case (int)Commands.RestartIIS:
						serviceName = ConfigurationManager.AppSettings["IISServiceName"];
						RestartService(serviceName);
						break;
					case (int)Commands.StartAppPool:
						StartApplicationPool(extraArgs.Trim());
						break;
					case (int)Commands.StopAppPool:
						StopApplicationPool(extraArgs.Trim());
						break;
					case (int)Commands.StartSQLJob:
						StartSQLJob(extraArgs);
						break;
					case (int)Commands.StartWinService:
						serviceName = extraArgs.Trim();
						StartService(serviceName);
						break;
					case (int)Commands.StopWinService:
						serviceName = extraArgs.Trim();
						StopService(serviceName);
						break;
					case (int)Commands.RestartWinService:
						serviceName = extraArgs.Trim();
						RestartService(serviceName);
						break;
					case (int)Commands.UpdateAppPoolList:
						GetAllApplicationPoolNames();
						break;
				}
				File.Delete(filePath);
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Restart any windows service as per service name
		private void RestartService(string serviceName)
		{
			var timeoutMilliseconds = 60000;
			ServiceController service = new ServiceController(serviceName);
			try
			{
				int millisec1 = Environment.TickCount;
				TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

				service.Stop();
				service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

				// count the rest of the timeout
				int millisec2 = Environment.TickCount;
				timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

				service.Start();
				service.WaitForStatus(ServiceControllerStatus.Running, timeout);

				Log($"{serviceName} restarted successfully");
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Start any windows service as per service name
		private void StartService(string serviceName)
		{
			try
			{
				ServiceController service = new ServiceController(serviceName);
				if (service.Status == ServiceControllerStatus.Stopped)
				{
					var timeoutMilliseconds = 60000;
					TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
					service.Start();
					service.WaitForStatus(ServiceControllerStatus.Running, timeout);
					Log($"{serviceName} started successfully");
				}
				else
				{
					Log($"{serviceName} already running");
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Stop any windows service as per service name
		private void StopService(string serviceName)
		{
			try
			{
				ServiceController service = new ServiceController(serviceName);
				if (service.Status == ServiceControllerStatus.Running)
				{
					var timeoutMilliseconds = 60000;
					TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
					service.Stop();
					service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
					Log($"{serviceName} stopped successfully");
				}
				else
				{
					Log($"{serviceName} already stopped");
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Recycle any application pool using poolname
		private void RecycleApplicationPool(string siteName)
		{
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					var applicationPools = iisManager.ApplicationPools;
					foreach (var applicationPool in applicationPools)
					{
						if (applicationPool.Name == siteName)
						{
							applicationPool.Recycle();
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Start any applicatin pool using poolname
		private void StartApplicationPool(string siteName)
		{
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					var applicationPools = iisManager.ApplicationPools;
					foreach (var applicationPool in applicationPools)
					{
						if (applicationPool.Name == siteName)
						{
							applicationPool.Start();
							Log($"{siteName} started successfully");
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Stop any application pool using poolname
		private void StopApplicationPool(string siteName)
		{
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					var applicationPools = iisManager.ApplicationPools;
					foreach (var applicationPool in applicationPools)
					{
						if (applicationPool.Name == siteName)
						{
							applicationPool.Stop();
							Log($"{siteName} stopped successfully");
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Get All Application Pool name
		private void GetAllApplicationPoolNames()
		{
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					var applicationPools = iisManager.ApplicationPools;
					var poolNameListString = string.Empty;
					foreach (var applicationPool in applicationPools)
					{
						poolNameListString += $"{applicationPool.Name + Environment.NewLine}";
					}
					UpdateApplicationPoolList(poolNameListString);
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}
		//Start any SQL job using jobname
		private void StartSQLJob(string jobName)
		{
			try
			{
				Server server = new Server(ConfigurationManager.AppSettings["SQLServerName"]);
				var sqlUserName = ConfigurationManager.AppSettings["SQLServerJobUserName"];
				if (!string.IsNullOrEmpty(sqlUserName))
				{
					server.ConnectionContext.LoginSecure = false;
					server.ConnectionContext.Login = sqlUserName;
					server.ConnectionContext.Password = ConfigurationManager.AppSettings["SQLServerJobPassword"];
				}
				server.JobServer.Jobs[jobName]?.Start();
				Log($"SQL Job - {jobName} started successfully");
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Save execution log
		private void Log(string text)
		{
			var logTextFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "log.txt");
			File.AppendAllText(logTextFilePath, $"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {text + Environment.NewLine}");
		}

		//Save error log
		private void ErrorLog(string text)
		{
			var logTextFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "errorlog.txt");
			File.AppendAllText(logTextFilePath, text + Environment.NewLine);
		}

		//Save Application pool list to text file
		private void UpdateApplicationPoolList(string text)
		{
			var applicationPoolListFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "applicationpoollist.txt");
			if (File.Exists(applicationPoolListFilePath))
			{
				File.Delete(applicationPoolListFilePath);
			}
			File.AppendAllText(applicationPoolListFilePath, text);
		}

		#endregion
	}
}
