# Service Management Tool
## Configuration Steps

### 1. Install Windows Service
- Create two folder - one for Log(`LogFolderFilePath`) and one for Service command(`ServiceCommandWatcherFolderPath`)
- Build Solution `ServiceManagementWindowsService.sln`
- Set key value in app.config.
    - `<add key="SQLServiceName" value="MSSQL$SQLEXPRESS" />` - SQL service name
    - `<add key="IISServiceName" value="W3SVC" />` - IIS service name
    - `<add key="LogFolderFilePath" value="E:\Templates" />` - Folder path where service execution logs saved
    - `<add key="ServiceCommandWatcherFolderPath" value="E:\\MailTemp" />` - Folder path where service command txt file save from website
    - `<add key="SQLServerName" value="ADMIN\MSSQLSERVER1" />` - SQL server name for run SQL job
    - `<add key="SQLServerJobUserName" value="sa" />` - SQL server username
    - `<add key="SQLServerJobPassword" value="sa123" />` - SQL server password
- Open cmd prompt with run as administrator
- Type below command with windows service exe path
- `sc create WindowsServiceManagementService binpath= "E:\Projects\WindowsService1\WindowsService1\bin\Debug\ServiceManagementWindowsService.exe"`
- `sc start WindowsServiceManagementService`

### 2. Run Website
- Build Solution `ServiceManagmentCore.sln`
- Set value in appsettings.json
    - `"ServiceCommandFolderPath": "E:\\MailTemp"` - Folder path where service command txt file save from website
    - `"LogFolderFilePath": "E:\\Templates"` - Folder path where service execution logs saved
    - `"LoginUsername": "admin"` - website credentails
    - `"LoginPassword": "123"`
- Run website or Host website to IIS
- For Host to IIS
    - open command prompt in `E:\Sourcing\ServiceManagementTool\ServiceManagmentCore\ServiceManagmentCore` folder
    - Run `dotnet publish` command
    - host `bin\Debug\netcoreapp2.2\publish` folder to IIS