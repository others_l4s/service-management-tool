﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ServiceManagmentCore.Models;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Web.Administration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.SqlServer.Management.Smo;
using System.ServiceProcess;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace ServiceManagmentCore.Controllers
{
	public class HomeController : Controller
	{
		private readonly AppSettings _appSettings;
		private const string _loginSessionKeyName = "_Username";
		private readonly ILogger<HomeController> _logger;
		public HomeController(ILogger<HomeController> logger, IOptions<AppSettings> options)
		{
			_appSettings = options.Value;
			_logger = logger;
		}

		public IActionResult Index()
		{
			var username = HttpContext.Session.GetString(_loginSessionKeyName);
			if (string.IsNullOrEmpty(username))
			{
				return RedirectToAction("Index", "Login");
			}

			GetSQLJobList();
			GetApplicationPoolList();
			GetWinServiceList();

			ViewData["IsLogin"] = true;
			string[] lines = new string[100];
			try
			{
				lines = System.IO.File.ReadAllLines(_appSettings.LogFolderFilePath + @"\log.txt");
				Array.Reverse(lines);
				lines = lines.Take(10).ToArray();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex.Message);
			}

			var model = new DataModel()
			{
				Logs = lines
			};
			return View(model);
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[HttpPost]
		public ActionResult SQLServiceStart()
		{
			PutCommand((int)Commands.StartSQL);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult SQLServiceStop()
		{
			PutCommand((int)Commands.StopSQL);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult SQLServiceRestart()
		{
			PutCommand((int)Commands.RestartSQL);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult IISServiceStart()
		{
			PutCommand((int)Commands.StartIIS);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult IISServiceStop()
		{
			PutCommand((int)Commands.StopIIS);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult IISServiceRestart()
		{
			PutCommand((int)Commands.RestartIIS);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ApplicationPoolRecycle(DataModel dataModel, string command)
		{
			if (command == "Start")
				PutCommand((int)Commands.StartAppPool, dataModel.ApplicationPoolName.Trim());
			else if (command == "Stop")
				PutCommand((int)Commands.StopAppPool, dataModel.ApplicationPoolName.Trim());
			else if (command == "UpdatePoolList")
				PutCommand((int)Commands.UpdateAppPoolList);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult SQLJobStart(DataModel dataModel)
		{
			PutCommand((int)Commands.StartSQLJob, dataModel.SQLJobName.Trim());
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult WindowsService(DataModel dataModel, string command)
		{
			if (command == "Start")
				PutCommand((int)Commands.StartWinService, dataModel.WindowsServiceName.Trim());
			else if (command == "Stop")
				PutCommand((int)Commands.StopWinService, dataModel.WindowsServiceName.Trim());
			else if (command == "Restart")
				PutCommand((int)Commands.RestartWinService, dataModel.WindowsServiceName.Trim());

			return RedirectToAction("Index", "Home");
		}
		public ActionResult Logout()
		{
			HttpContext.Session.Remove(_loginSessionKeyName);
			return RedirectToAction("Index", "Login");
		}

		#region ' Private Methods'

		private void PutCommand(int command, string extraArgument = "")
		{
			Guid guid = Guid.NewGuid();
			var path = _appSettings.ServiceCommandFolderPath + @"\" + guid.ToString() + ".txt";
			System.IO.File.AppendAllText(path, command.ToString() + " " + extraArgument);
		}

		private void GetSQLJobList()
		{
			List<string> list = new List<string>();
			try
			{
				Server server = new Server(_appSettings.SQLServerName);
				if (!string.IsNullOrEmpty(_appSettings.SQLServerJobUserName))
				{
					server.ConnectionContext.LoginSecure = false;
					server.ConnectionContext.Login = _appSettings.SQLServerJobUserName;
					server.ConnectionContext.Password = _appSettings.SQLServerJobPassword;
				}
				var jobs = server.JobServer.Jobs;
				foreach (var item in jobs)
				{
					list.Add(item.ToString());
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex.Message);
			}
			ViewBag.ListOfSQLJob = list.OrderBy(x => x).ToList();
		}

		private void GetWinServiceList()
		{
			List<string> list = new List<string>();
			try
			{
				ServiceController[] services = ServiceController.GetServices();
				foreach (var item in services)
				{
					list.Add(item.ServiceName);
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex.Message);
			}
			ViewBag.ListOfWindowsService = list.OrderBy(x => x).ToList();
		}

		private void GetApplicationPoolList()
		{
			List<string> list = new List<string>();
			try
			{
				list = System.IO.File.ReadAllLines(_appSettings.LogFolderFilePath + @"\applicationpoollist.txt").OrderBy(x => x).ToList();
			}
			catch
			{

			}
			ViewBag.ListOfApplicationPool = list;
		}

		#endregion
	}
}
