﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ServiceManagmentCore.Models;

namespace ServiceManagmentCore.Controllers
{
	public class LoginController : Controller
	{
		private readonly AppSettings _appSettings;
		private const string LoginSessionKeyName = "_Username";

		public LoginController(IOptions<AppSettings> options)
		{
			_appSettings = options.Value;
		}

		public IActionResult Index()
		{
			var username = HttpContext.Session.GetString(LoginSessionKeyName);
			if (!string.IsNullOrEmpty(username))
			{
				return RedirectToAction("Index", "Home");
			}
			ViewData["IsLogin"] = false;
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(LoginModel objUser)
		{
			if (ModelState.IsValid)
			{
				if (objUser.UserName == _appSettings.LoginUsername && objUser.Password == _appSettings.LoginPassword)
				{
					HttpContext.Session.SetString(LoginSessionKeyName, objUser.UserName);
					return RedirectToAction("Index", "Home");
				}
				else
				{
					ModelState.AddModelError(string.Empty, "Username or password is wrong.");
				}
			}
			return View("Index", objUser);
		}
	}
}