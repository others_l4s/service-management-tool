﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceManagmentCore.Models
{
	public class AppSettings
	{
		public string ServiceCommandFolderPath { get; set; }
		public string LogFolderFilePath { get; set; }
		public string LoginUsername { get; set; }
		public string LoginPassword { get; set; }

		public string SQLServerName { get; set; }
		public string SQLServerJobUserName { get; set; }
		public string SQLServerJobPassword { get; set; }
	}
}
