﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceManagmentCore.Models
{
	public enum Commands
	{
		StartSQL,
		StopSQL,
		RestartSQL,
		StartIIS,
		StopIIS,
		RestartIIS,
		RecycleAppPool,
		StartSQLJob,
		StartAppPool,
		StopAppPool,
		StartWinService,
		StopWinService,
		RestartWinService,
		UpdateAppPoolList
	}
}
