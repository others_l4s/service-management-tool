﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceManagmentCore.Models
{
	public class DataModel
	{
		public string ApplicationPoolName { get; set; }
		public string SQLJobName { get; set; }
		public string WindowsServiceName { get; set; }
		public string[] Logs { get; set; }
	}
}
